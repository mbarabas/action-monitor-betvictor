**RUN:**

mvn spring-boot:run

**PACKAGE:**

mvn -Pprod package

**REST endpoints:**

http://localhost:8080/api/app/health

http://localhost:8080/api/app/version

**Websocket DB notify (open both):**

http://localhost:8080/index.html  --- Click connect!

http://localhost:8080/person.html --- Add/update/remove Person!

If you want to try out the latest version using ActiveMQ as a message broker, you have to setup a broker on your localhost listening on port 61613 for STOMP messages.
(This can be done easily with using Vagrant and leverage the Vagrantfile & provision.sh files)
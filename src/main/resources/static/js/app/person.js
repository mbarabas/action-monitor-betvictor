define(["jquery", "datatables"], function($) {
	console.log('person.js loaded');


	function addPerson() {
		var personData = {
//				'id' 		: $('input[name=idInDBField]').val(),
				'firstName' : $('input[id=firstNameField]').val(),
				'lastName'	: $('input[id=lastNameField]').val(),
		};

		console.log(JSON.stringify(personData));

		$.ajax({
			type        : "POST", 		// define the type of HTTP verb we want to use
			url         : "api/person", // the url 
			data        : JSON.stringify(personData), 	// our data object
			dataType    : "json", 		// expected data from the server
			contentType : "application/json",
			success: function(data){
				alert('Person added successfully: ' + JSON.stringify(data));
				getAllPerson();
			}
		});

		console.log("end of addPerson()");
	}
	
	function updatePerson() {
		var personData = {
				'id' 		: $('input[id=idInDBField]').val(),
				'firstName' : $('input[id=firstNameField]').val(),
				'lastName'	: $('input[id=lastNameField]').val(),
		};

		console.log(JSON.stringify(personData));

		$.ajax({
			type        : "PUT", 		// define the type of HTTP verb we want to use
			url         : "api/person", // the url 
			data        : JSON.stringify(personData), 	// our data object
			dataType    : "json", 		// expected data from the server
			contentType : "application/json",
			success: function(data){
				alert('Person updated successfully: ' + JSON.stringify(data));
				getAllPerson();
			}
		});

		console.log("end of updatePerson()");
	}
	
	function removePerson() {

		var id = $('input[id=idInDBField]').val();
		
		$.ajax({
			type        : "DELETE", 		// define the type of HTTP verb we want to use
			url         : "api/person/"+id, // the url 
			success: function(data){
				alert('Person removed successfully (ID: ' + id + ')');
				getAllPerson();
			}
		});


		console.log("end of removePerson()");
	}
	
	function checkIDAvailability() {
		var id = $('input[id=idInDBField]').val();
		var available = (typeof id !== "undefined" && id.length > 0);
		
//		if (!available) {
//			alert("NO ID WAS GIVEN...");
//		}
			
		return available;
	}


	function getAllPerson() {
		$('#response_table').DataTable( {
		    ajax: {
		        url: 'api/person',
		        dataSrc: ''
		    },
		    columns: [ 
				{data: "id", 		 title: "ID"},
				{data: "firstName",	 title: "FIRST NAME"},
				{data: "lastName",	 title: "LAST NAME" } 
			],
			bDestroy: true
		});
		
		console.log("end of getAllPerson()");
	}



	document.getElementById('addPersonButton').addEventListener('click', function () {
		var idAvailable = checkIDAvailability(); 
		console.log(idAvailable);
		if (idAvailable) {			
			alert("CANNOT ADD NEW PERSON WITH SPECIFIED ID...");
		}
		else {
			addPerson();
		}
	}, false);

	document.getElementById('updatePersonButton').addEventListener('click', function () {
		var idAvailable = checkIDAvailability();
		if (idAvailable) {
			updatePerson();
		}
		else {
			alert("NO ID WAS GIVEN...");
		}
	}, false);
	
	document.getElementById('removePersonButton').addEventListener('click', function () {
		var idAvailable = checkIDAvailability();
		if (idAvailable) {			
			removePerson();
		}
		else {
			alert("NO ID WAS GIVEN...");
		}
	}, false);

	
	getAllPerson();

});
define(["jquery", "stomp", "sockjs", "datatables"], function($) {

	console.log('[!] Loaded main.js');
	
	var stompClient = null;

	function setConnected(connected) {
		document.getElementById('connectButton').disabled = connected;
		document.getElementById('disconnectButton').disabled = !connected;
		document.getElementById('conversationDiv').style.visibility = connected ? 'visible' : 'hidden';
		document.getElementById('response').innerHTML = '';
	}

	function connect() {
		var loc = window.location;
		var url = '//' + loc.host + '/websocket/dbtracker';
		var socket = new SockJS(url);
		stompClient = Stomp.over(socket);
		stompClient.connect({}, function(frame) {
			setConnected(true);
			console.log('Connected: ' + frame);
			stompClient.subscribe('/topic/dbtracker', function(message){
				showGreeting(message.body);
//				onMessage(JSON.parse(message));
			});
		});
	}

	function onMessage(message) {
		console.log("why MEEE??? " + message);
	}

	function disconnect() {
		if (stompClient != null) {
			stompClient.disconnect();
		}
		setConnected(false);
		console.log("Disconnected");
	}

//	function sendName() {
//		var name = document.getElementById('name').value;
//		stompClient.send("/topic/dbtracker", {}, JSON.stringify({ 'name': name })); 
//	}

	function showGreeting(message) {
		var response = document.getElementById('response');
		var p = document.createElement('p');
		p.style.wordWrap = 'break-word';
		p.appendChild(document.createTextNode(message));
		response.appendChild(p);
	}
	
	
	document.getElementById('connectButton').addEventListener('click', function () {
		connect();
	}, false);

	document.getElementById('disconnectButton').addEventListener('click', function () {
		disconnect();
	}, false);

	

});
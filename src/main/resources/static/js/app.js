// Place third party dependencies in the lib folder
//
// Configure loading modules from the lib directory,
// except 'app' ones, 
requirejs.config({
    "baseUrl": "js/lib",
    "paths": {
      "app": "../app",
//      "jquery": "//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min",
      "jquery": "jquery-2.1.4.min",
      "sockjs": "sockjs-0.3.4.min",
      "stomp": "stomp.min",
      "datatables": "jquery.dataTables.min",
      "datatables-bootstrap": "dataTables.bootstrap.min"
    },
    "shim": {
//        "jquery.cookie": ["jquery"],
//        "bootstrap": ['jquery'],
//        "bootstrap-timepicker" : ['jquery', 'bootstrap'],
        "datatables": ['jquery'],
//        "datatables-bootstrap": ['datatables', 'bootstrap'],
//        "jqueryui": ['jquery'],
//        "general": ['jquery', 'bootstrap']
    }
});

// Load the main app module to start the app
//requirejs(["app/main"]);
//requirejs(["app/person"]);
//requirejs(["datatables"]);

package com.betvictor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BetvictorActionMonitorApplication {

    public static void main(String[] args) {
        SpringApplication.run(BetvictorActionMonitorApplication.class, args);
    }
}

package com.betvictor.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import com.betvictor.aop.notify.NotifyAspect;

@Configuration
@EnableAspectJAutoProxy
public class NotifyAspectConfiguration {

    @Bean
    public NotifyAspect notifyAspect() {
        return new NotifyAspect();
    }
}

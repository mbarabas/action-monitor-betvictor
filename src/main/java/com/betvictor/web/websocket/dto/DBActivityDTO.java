package com.betvictor.web.websocket.dto;

/**
 * DTO for storing a database activity.
 */
public class DBActivityDTO {


    private String value;
    private String id;
    private String command;
    private String time;


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getID() {
        return id;
    }

    public void setID(String ipAddress) {
        this.id = ipAddress;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "DBActivityDTO{" +
            ", id='" 		+ id + '\'' +
            ", value='" 	+ value + '\'' +
            ", command='" 	+ command + '\'' +
            ", time='" 		+ time + '\'' +
            '}';
    }
}

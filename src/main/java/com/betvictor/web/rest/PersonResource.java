package com.betvictor.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.betvictor.aop.notify.NotifyClients;
import com.betvictor.model.Person;
import com.betvictor.repository.PersonRepository;

@RestController
@RequestMapping("/api")
public class PersonResource {
	
	private final Logger log = LoggerFactory.getLogger(AppVersionResource.class);

    @Autowired
    private PersonRepository personRepository;
	
	 /**
     * POST  /person -> Create a new Person.
     */
    @NotifyClients
    @RequestMapping(value = "/person",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> createPerson(@RequestBody Person person) throws URISyntaxException {
        log.debug("REST request to create Person: {}", person);
        if (person.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new Person cannot already have an ID").body(null);
        }
        Person result = personRepository.save(person);
        return ResponseEntity.created(new URI("/api/person" + result.getId()))
            .body(result);
    }

    /**
     * PUT  /person -> Updates an existing Person.
     */
    @NotifyClients
    @RequestMapping(value = "/person",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Person> updateAppVersion(@RequestBody Person person) throws URISyntaxException {
        log.debug("REST request to update Person: {}", person);
        if (person.getId() == null) {
            return createPerson(person);
        }
        Person result = personRepository.save(person);
        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * DELETE  /person/:id -> delete the "id" appVersion.
     */
    @NotifyClients
    @RequestMapping(value = "/person/{id}",
    		method = RequestMethod.DELETE,
    		produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> deletePerson(@PathVariable Long id) {
    	log.debug("REST request to delete Person: {}", id);
    	personRepository.delete(id);
    	
    	return ResponseEntity
    			.ok()
    			.build();
    }

    /**
     * GET  /person -> get all Person.
     */
    @RequestMapping(value = "/person",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Person> getPersonList() {
        log.debug("REST request to get all Person");
        return personRepository.findAll();
    }
    
}

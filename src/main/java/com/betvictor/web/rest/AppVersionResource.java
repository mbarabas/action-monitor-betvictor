package com.betvictor.web.rest;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/app")
public class AppVersionResource {
	
    private final Logger log = LoggerFactory.getLogger(AppVersionResource.class);

	/**
     * GET  /api/app/version -> get the app version.
     */
    @RequestMapping(value = "/version",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    public String getAppVersionFromManifest() {
        log.debug("REST request to get app version from MANIFEST file");
        String version = null;
        
        version = getClass().getPackage().getImplementationVersion();  
        if (version == null) {
            Properties prop = new Properties();
            try {
                prop.load(this.getClass().getResourceAsStream("/META-INF/MANIFEST.MF"));
                version = prop.getProperty("Implementation-Version");
            }
            catch (IOException e) {
                log.error(e.toString());
            }
        }
        log.info("App version: "+version);        
        
        return version;
    }
}

package com.betvictor.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing App health.
 */
@RestController
@RequestMapping("/api/app")
public class AppHealthResource {

    private final Logger log = LoggerFactory.getLogger(AppHealthResource.class);

    /**
     * GET  /api/app/health -> get health status of the app.
     */
    @RequestMapping(value = "/health",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    public String getAppHealth() {
        log.debug("REST request to get App health status");
        return "OK!";
    }

}

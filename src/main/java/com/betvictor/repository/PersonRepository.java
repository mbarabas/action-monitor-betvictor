package com.betvictor.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.betvictor.model.Person;

public interface PersonRepository extends JpaRepository<Person, Long> {

//	List<Person> findByLastName(@Param("name") String name);

}

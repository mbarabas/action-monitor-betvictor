package com.betvictor.aop.notify;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.betvictor.model.Person;
import com.betvictor.web.websocket.dto.DBActivityDTO;

@Aspect
public class NotifyAspect {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private SimpMessagingTemplate template;

    private static final String WEBSOCKET_TOPIC = "/topic/dbtracker";
    
    private static String cmd;
    
    @Autowired
    private Environment env;


    
    @Pointcut("@annotation(com.betvictor.aop.notify.NotifyClients)")
    public void notifyPointcut() {}

//    @Pointcut("execution(* com.betvictor.web.rest.**.*(..))")
//    @Pointcut("within(com.betvictor.web.rest..*)")
//    public void methodPointcut() {}
    

    @Around("notifyPointcut()")
    public Object notifyAround(ProceedingJoinPoint joinPoint) throws Throwable {
//        if (log.isDebugEnabled()) {
            log.info("Enter: {}.{}() with argument[s] = {}", joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(), Arrays.toString(joinPoint.getArgs()));
//        }
        try {
            Object result = joinPoint.proceed();
//            if (log.isDebugEnabled()) {
                log.info("Exit: {}.{}() with result = {}", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), result);
//            }
            return result;
        } catch (IllegalArgumentException e) {
            log.error("Illegal argument: {} in {}.{}()", Arrays.toString(joinPoint.getArgs()),
                    joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());

            throw e;
        }
    }


    @After("notifyPointcut() &&" +
    		"args(result)") 
    public void notifyClients(JoinPoint joinPoint, Person result) throws Throwable {
    	if (joinPoint.getSignature().getName().toLowerCase().contains("CREATE".toLowerCase())) {
    		cmd = "CREATE";
    	}
    	else if (joinPoint.getSignature().getName().toLowerCase().contains("UPDATE".toLowerCase())) {
    		cmd = "UPDATE";
    	}
//    	else if (joinPoint.getSignature().getName().toLowerCase().contains("DELETE".toLowerCase())) {
//    		cmd = "DELETE";
//    	}
    	
    	DBActivityDTO dbActivityDTO = new DBActivityDTO();
    	dbActivityDTO.setID(result.getId().toString());
    	dbActivityDTO.setValue(result.getFullName());
    	dbActivityDTO.setCommand(cmd);
    	Instant instant = Instant.ofEpochMilli(Calendar.getInstance().getTimeInMillis());
        dbActivityDTO.setTime(dateTimeFormatter.format(ZonedDateTime.ofInstant(instant, ZoneOffset.systemDefault())));
        template.convertAndSend(WEBSOCKET_TOPIC, dbActivityDTO);
    }
    
    @After("notifyPointcut() &&" +
    		"args(id)") 
    public void notifyClients(JoinPoint joinPoint, Long id) throws Throwable {
    	if (joinPoint.getSignature().getName().toLowerCase().contains("DELETE".toLowerCase())) {
    		cmd = "DELETE";
    	}
    	
    	DBActivityDTO dbActivityDTO = new DBActivityDTO();
    	dbActivityDTO.setID(id.toString());
    	dbActivityDTO.setValue("");
    	dbActivityDTO.setCommand(cmd);
    	Instant instant = Instant.ofEpochMilli(Calendar.getInstance().getTimeInMillis());
        dbActivityDTO.setTime(dateTimeFormatter.format(ZonedDateTime.ofInstant(instant, ZoneOffset.systemDefault())));
        template.convertAndSend(WEBSOCKET_TOPIC, dbActivityDTO);
    }
  
    
}

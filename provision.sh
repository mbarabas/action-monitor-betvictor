#!/bin/bash

echo "##### PROVISION SCRIPT STARTED #####"

apt-get update
apt-get -y install activemq


echo '<beans
  xmlns="http://www.springframework.org/schema/beans"
  xmlns:amq="http://activemq.apache.org/schema/core"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.0.xsd
  http://activemq.apache.org/schema/core http://activemq.apache.org/schema/core/activemq-core.xsd">

    <bean class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer"/>

    <broker xmlns="http://activemq.apache.org/schema/core"
            useJmx="false"
            brokerName="localhost"
            dataDirectory="${activemq.base}/data">

        <persistenceAdapter>
           <kahaDB directory="${activemq.base}/data/kahadb"/>
        </persistenceAdapter>

		<transportConnectors>
		   <transportConnector name="stomp" uri="stomp://0.0.0.0:61613"/>
		</transportConnectors>

    </broker>

</beans>' > /etc/activemq/instances-enabled/main

# ln -s /etc/activemq/instances-available/main /etc/activemq/instances-enabled/main
service activemq restart